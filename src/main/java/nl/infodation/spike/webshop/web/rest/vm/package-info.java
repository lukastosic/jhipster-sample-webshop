/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.infodation.spike.webshop.web.rest.vm;
